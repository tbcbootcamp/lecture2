package com.example.firslecturetbc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonForRandom.setOnClickListener {
            if (randomPicker()) {
                Toast.makeText(this, "The number is even", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "The number is odd", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun randomPicker(): Boolean {
        val randomNumber = (1..101).random()
        return randomNumber % 2 == 0
    }
}
